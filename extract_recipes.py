#!/usr/bin/env python3

from bs4 import BeautifulSoup
import json
import sys
import traceback
from datetime import datetime
import os

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def extract_from_file(file):
    try:
        with open(file) as inputHtml:
            return extract(inputHtml)
    except Exception as e:
        eprint(traceback.format_exc())
        eprint("file was {}".format(file))


def extract(inputHtml):
    soup = BeautifulSoup(inputHtml, "html.parser")

    recipeJsonText = soup.body["data-tracking"]
    recipe = json.loads(recipeJsonText)

    for scrtag in soup.find_all("script", type="application/ld+json"):
        aggregationInfo = json.loads(scrtag.get_text())

    for userDetails in soup.select("div.user-details p"):
        memberSince = userDetails.get_text().replace("\n", "", 1).replace("\t", "").splitlines()[0].replace("Mitglied seit ", "")

    aggregationInfo["author"]["memberSince"] = memberSince

    comments = []
    for commentDiv in soup.select("div.comment-child, div.comment"):

        comment = {}

        if "comment-child" in commentDiv["class"]:
            for sibling in commentDiv.find_previous_siblings():
                if "comment" in sibling["class"]:
                    comment["refersToId"] = sibling.select(".comment-text")[0]["id"]
                    break

        commentText = commentDiv.select(".comment-text")[0]
        comment["content"] = commentText.get_text()
        comment["id"] = commentText["id"]

        comment["isHelpful"] = "topkommentar" in commentDiv["class"]

        authorLinks = commentDiv.find("div", "comment-author").find_all("a")
        if len(authorLinks) > 1:
            comment["author"] = authorLinks[1].get_text()
        else:
            comment["author"] = authorLinks[0].previous_sibling.strip("\n")

        comment["date"] = commentDiv.find("div", "comment-author").find("p").get_text().replace(" Uhr", "")

        if comment["id"] not in [c["id"] for c in comments]:
            comments.append(comment)

    recipePage = {
        "recipe": recipe,
        "meta": aggregationInfo,
        "comments": sorted(comments, key=lambda x: datetime.strptime(x["date"], '%d.%m.%Y %H:%M'))
    }

    return recipePage


dirOrFileToRead = sys.argv[1]
startWithFilenameMatching = ""

if len(sys.argv) == 3:
    startWithFilenameMatching = sys.argv[2]

if os.path.isdir(dirOrFileToRead):
    dirlist = sorted(os.listdir(dirOrFileToRead))
    if startWithFilenameMatching == "":
        startWithFound = True
    else:
        startWithFound = False

    totalCount = len(dirlist)
    currentIndex = 0
    for dirEntry in dirlist:
        currentIndex += 1
        if startWithFilenameMatching in dirEntry:
            startWithFound = True

        if startWithFound == True:
            filename = "{}/{}".format(dirOrFileToRead, dirEntry)
            eprint("{}/{} ({}%) : {}".format(currentIndex, totalCount, round(100 * currentIndex / totalCount, 1), filename))
            extracted = extract_from_file(filename)
            if extracted != None:
                print(json.dumps(extracted))
else:
    extracted = extract_from_file(dirOrFileToRead)
    if extracted != None:
        jsonDump = json.dumps(extracted, indent=4)
        print(jsonDump)
