#!/usr/bin/env python3

import sys
import re
import json
import pprint
from neo4j.v1 import GraphDatabase

create_recipe = """
unwind {recipeDocs} as recipeDoc
with recipeDoc.recipe.content as recipe, recipeDoc.meta as meta, recipeDoc.comments as comments

merge (r:Recipe{id: recipe.id})
  on create set
    r.name = meta.name,
    r.description = meta.description,
    r.datePublished = meta.datePublished,
    r.image = meta.image,
    r.cookTime = meta.cookTime,
    r.prepTime = meta.prepTime,
    r.recipeInstructions = meta.recipeInstructions,
    r.recipeYield = meta.recipeYield

merge (a:Person{name: meta.author.name})
  on create set
    a.memberSince = meta.author.memberSince

merge (r)<-[:PUBLISHES]-(a)

foreach(ingredient in recipe.ingredients |
  merge (f:Food{id: ingredient.food.id})
    on create set
      f.name = ingredient.food.name

  merge (r)-[rel:HAS_INGREDIENT]->(f)
    on create set
      rel.unit = ingredient.unit,
      rel.amount = ingredient.amount,
      rel.usageInfo = ingredient.usageInfo
)

foreach( category in meta.recipeCategory |
  merge (c:Category{category: category})
  merge (r)-[:HAS_CATEGORY]->(c)
)

foreach(comment in comments |
  merge (c:Comment{id: comment.id})
    on create set
      c.content = comment.content,
      c.isHelpful = comment.isHelpful,
      c.date = comment.date

  foreach (refId in (case when exists(comment.refersToId) then [comment.refersToId] else [] end) |
    merge (refc:Comment{id:refId})
    merge (refc)<-[:REFERS_TO]-(c)
  )

  merge (ca:Person{name: comment.author})
  merge (ca)-[:WRITES_COMMENT]->(c)
  merge (r)-[:HAS_COMMENT]->(c)
)
"""

def save_to_graph(recipes):
  result = session.run(create_recipe, {"recipeDocs": recipes})
  for x in result:
    print(x)



driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "neo"))
print('GraphDatabase driver initialized')

session = driver.session()

recipes = []
with open(sys.argv[1]) as infile:
  print("> opened file")
  for line in infile:
    if len(recipes) < 10:
      recipe = json.loads(line)
      print("{}".format(recipe["meta"]["name"]))
      recipes.append(recipe)
    else:
      print("> saving batch of {} recipes\n".format(len(recipes)))
      save_to_graph(recipes)
      recipes.clear()

  recipesLen = len(recipes)
  if recipesLen > 0:
    print("> saving remaining batch of last {} recipes\n".format(recipesLen))
    save_to_graph(recipes)

