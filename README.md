# CK import

## method 1 : shell commands for recipe collection

### prerequisites:
* curl
* pup (https://github.com/ericchiang/pup)
* python3
* python3 bs4 (`python3 -m pip install --user bs4`)
* python3 neo4j-driver (`python3 -m pip install --user neo4j-driver`)


### gather all links to recipes:

	for x in $(seq 0 30 350000); do
    	echo $x > /dev/stderr;
    	curl -s https://www.chefkoch.de/rs/s${x}o3/Rezepte.html | pup ' .search-list-item > a attr{href}';
	done > links

### remove possible duplicates:

	cat links | sort | uniq > all_links
	mkdir recipes

### download recipes (~56GB)

	./scrape.sh all_links

### extract JSON from them

	python3 extract_recipes.py > recipes_raw.json


## method 2: download ~240k recipes JSON

### download compressed JSON file (144MB)
	curl -O http://binarchy.net/recipes_raw.json.bz2

### decompress JSON file (~1.1GB)
	bunzip2 recipes_raw.json.bz2

### serve the json via http on port 8080

	python -m SimpleHTTPServer 8080


## import recipes to Neo4j (desktop)

download and install [neo4j desktop](https://neo4j.com/download/). 

* create a new database
* install the APOC plugin
* click "manage" 
* hit the "start" button
* open the Terminal tab
* run the following command, wait and enjoy.


	`bin/neo4j-shell < PATH_TO_PROJECT/bulkimport_apoc_periodic_iterate.cql`

