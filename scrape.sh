#!/bin/bash

cat $1 | while read x; do \
	F=$(echo $x | sed 's/\//_/g; s/_rezepte_//');
	echo $x;
	(curl -s https://www.chefkoch.de${x} > recipes/$F) &
	sleep 0.1;
done
